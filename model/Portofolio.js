const mongoose = require('mongoose');

const portofolioSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    img: {
        type: String
    },
    description: {
        type: String
    },
    source_code: {
        type: String
    }
});

module.exports = mongoose.model('Portofolio', portofolioSchema);
