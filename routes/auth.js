const router = require('express').Router();
const User = require('../model/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { registerValidation, loginValidation } = require('../validation');

// register
router.post('/register', async (req, res) => {
    const { error } = registerValidation(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    // check if the user is already exist
    const emailExist = await User.findOne({
        email: req.body.email
    });
    if (emailExist) {
        return res.status(400).send('"email" already exists');
    }

    // hash password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    // create new user
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
    });

    try {
        const saved = await user.save();
        res.send({ user: user.id });
    } catch (error) {
        res.status(400).send(console.error);
    }
});

// login
router.post('/login', async (req, res) => {
    const { error } = loginValidation(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    // check if the user is already exist
    const user = await User.findOne({
        email: req.body.email
    });
    if (!user) {
        return res.status(400).send('User with the email could not be found');
    }

    // check password
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if (!validPass) {
        return res.status(400).send('Invalid password');
    }

    // create token
    const token = jwt.sign({ _id: user.id }, process.env.TOKEN_SECRET);
    res.header('auth-token', token).send(token);
});

module.exports = router;
