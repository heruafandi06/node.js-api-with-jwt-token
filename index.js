const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongoouse = require('mongoose');

// import routes
const authRoute = require('./routes/auth');
const portofolioRoute = require('./routes/portofolio');

dotenv.config();

// connect db
mongoouse.connect(
    process.env.DB_CONNECT,
    { useNewUrlParser: true },
    () => console.log('connected to db!')
);

// middleware
app.use(express.json());

// route middleware
app.use('/api', authRoute);
app.use('/api/portofolio', portofolioRoute);

app.listen(3000, () => console.log('Server up and running'));
